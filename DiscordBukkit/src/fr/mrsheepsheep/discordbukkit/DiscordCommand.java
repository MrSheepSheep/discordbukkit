package fr.mrsheepsheep.discordbukkit;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import me.itsghost.jdiscord.talkable.Group;

public class DiscordCommand implements CommandExecutor {

	DiscordBukkit plugin;

	public DiscordCommand(DiscordBukkit plugin){
		plugin.getCommand("discord").setExecutor(this);
		this.plugin = plugin;
	}

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		if (args.length == 0){
			sendHelp(sender);
		}
		else {
			if (args.length == 2 && args[0].equalsIgnoreCase("join")){
				if (sender instanceof Player){
					Player p = (Player) sender;

					if (plugin.getDiscord().getChannelID(args[1]) != null){
						sender.sendMessage(ChatColor.GREEN + "You are now speaking in channel " + args[1]);
						plugin.getDiscord().joinChannel(p.getUniqueId(), args[1]);
					}
					else
					{
						sender.sendMessage(ChatColor.RED + "This channel does not exist. Type /discord channels to see available channels.");
					}
				}
				else
				{
					sender.sendMessage("To send a message on a channel as console, use /discord send <channel> <message>");
				}
			}
			else if (args.length == 1 && args[0].equalsIgnoreCase("channels")){
				sender.sendMessage(ChatColor.YELLOW + "List of available channels: ");
				for (Group g : plugin.getDiscord().getDiscordServer().getGroups()){
					sender.sendMessage(ChatColor.ITALIC + " #" + g.getName() + " (" + plugin.getDiscord().numberIn(g.getId()) + ")");
				}
			}
			else if (args.length == 1 && args[0].equalsIgnoreCase("logout")){
				if (sender instanceof Player){
					Player p = (Player) sender;
					if (plugin.getDiscord().loggedInPlayers.containsKey(p.getUniqueId())){
						plugin.getDiscord().loggedInPlayers.get(p.getUniqueId()).stop();
						plugin.getDiscord().loggedInPlayers.remove(p.getUniqueId());
						p.sendMessage(ChatColor.GREEN + "Logged out successfuly.");
					}
					else
						p.sendMessage(ChatColor.RED + "You are not logged in to Discord.");
				}
			}
			else if (args.length > 2 && args[0].equalsIgnoreCase("send")){
				if (sender.hasPermission("discordbukkit.send")){
					String channel = args[1];
					if (plugin.getDiscord().getChannelID(channel) != null){
						String message = "";
						for (int i = 2; i < args.length; i++){
							message += args[i];
						}

						plugin.getDiscord().sendMessage(sender, channel, message);
						sender.sendMessage(ChatColor.GREEN + "Message sent!");
					}
					else
						sender.sendMessage(ChatColor.RED + "This channel does not exist. Type /discord channels to see available channels.");
				}
				else
					sender.sendMessage(ChatColor.RED + "You don't have permission.");
			}
		}
		return true;
	}

	private void sendHelp(CommandSender sender) {
		sender.sendMessage("/dis channels - List available channels");
		sender.sendMessage("/dis send <channel> <message> - Sends message to specified channel");
		if (sender instanceof Player){
			sender.sendMessage("/dis join <channel> - Joins specified channel");
			sender.sendMessage("/dis login <email> <password> - Login to Discord");
			sender.sendMessage("/dis logout - Logout from Discord");
		}
	}

}
