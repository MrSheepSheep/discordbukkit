package fr.mrsheepsheep.discordbukkit.test;

import me.itsghost.jdiscord.DiscordAPI;
import me.itsghost.jdiscord.event.EventListener;
import me.itsghost.jdiscord.events.UserChatEvent;

public class ExampleListener implements EventListener {
	DiscordAPI api;
	public ExampleListener(DiscordAPI api){
		this.api = api;
	}
	public void userChat(UserChatEvent e){
		if (e.getServer().getId().equalsIgnoreCase(Test.serverId)){
			System.out.println((e.getMsg().isEdited() ? "# " : "") + "[" + e.getServer().getName() + " #" + e.getGroup().getName() + "] " + e.getUser() + " > " + e.getMsg().getMessage());
		}
	}
}