package fr.mrsheepsheep.discordbukkit;

import org.bukkit.entity.Player;

import me.itsghost.jdiscord.Server;
import me.itsghost.jdiscord.event.EventListener;
import me.itsghost.jdiscord.events.APILoadedEvent;
import me.itsghost.jdiscord.events.ChannelDeletedEvent;
import me.itsghost.jdiscord.events.UserChatEvent;
import me.itsghost.jdiscord.events.UserJoinedChat;
import me.itsghost.jdiscord.talkable.Group;
import net.md_5.bungee.api.ChatColor;

public class DiscordChatListener implements EventListener {

	DiscordBukkit plugin;

	public DiscordChatListener(DiscordBukkit plugin){
		this.plugin = plugin;
	}

	public void onReadyAPI(APILoadedEvent e){
		if (plugin.getDiscord().api != null && plugin.getDiscord().api.isLoaded()){
			plugin.getDiscord().api.getSelfInfo().setUsername("Chat SLS");

			if (plugin.getDiscord().defaultChannel.isEmpty()){
				plugin.getDiscord().defaultChannel = plugin.getDiscord().getDiscordServer().getGroups().get(0).getName();
				plugin.getLogger().warning("Default channel will be first channel available: #" + plugin.getDiscord().getDiscordServer().getGroups().get(0).getName());
			}


			plugin.getLogger().info("Logged in as " + plugin.getDiscord().api.getSelfInfo().getUsername());
			plugin.getLogger().info("Tracking server named " + plugin.getDiscord().api.getServerById(plugin.getDiscord().serverId).getName());
			plugin.getLogger().info("Channels:");
			for (Group group : plugin.getDiscord().api.getServerById(plugin.getDiscord().serverId).getGroups()){
				plugin.getLogger().info(" #" + group.getName() + " (" + group.getId() + ")");

				if (group.getName().equalsIgnoreCase(plugin.getDiscord().defaultChannel)){
					plugin.getDiscord().defaultChannelId = group.getId();
				}
			}
		}
	}

	public void onChat(UserChatEvent e){
		if (isServer(e.getServer())){
			if (plugin.getDiscord().toIgnore != null && !e.getMsg().getMessage().contains(plugin.getDiscord().toIgnore)){
				for (Player p : plugin.getServer().getOnlinePlayers()){
					if (plugin.getDiscord().joinedChannels.containsKey(p.getUniqueId())){
						if (plugin.getDiscord().joinedChannels.get(p.getUniqueId()).equalsIgnoreCase(e.getGroup().getId())){
							p.sendMessage("<" + e.getUser().getUser().getUsername() + "> " + e.getMsg().getMessage());
						}
					}
				}
			}
			else if (plugin.getDiscord().toIgnore == null){
				for (Player p : plugin.getServer().getOnlinePlayers()){
					if (plugin.getDiscord().joinedChannels.containsKey(p.getUniqueId())){
						if (plugin.getDiscord().joinedChannels.get(p.getUniqueId()).equalsIgnoreCase(e.getGroup().getId())){
							p.sendMessage("<" + e.getUser().getUser().getUsername() + "> " + e.getMsg().getMessage());
						}
					}
				}
			}
			else
				plugin.getDiscord().toIgnore = null;
		}
	}

	public void onJoin(UserJoinedChat e){
		if (isServer(e.getServer())){
			plugin.getServer().broadcastMessage("[+] " + e.getGroupUser().getUser().getUsername());
		}
	}

	/* Redirection vers le channel par d�faut si supprim� */	
	public void onChannelDelete(ChannelDeletedEvent e){
		for (Player p : plugin.getServer().getOnlinePlayers()){
			if (plugin.getDiscord().getPlayerGroup(p.getUniqueId()).equals(e.getGroup())){
				plugin.getDiscord().joinChannel(p.getUniqueId(), plugin.getDiscord().defaultChannel);
				p.sendMessage(ChatColor.RED + "Your active channel was deleted. You were moved to the default channel #" + plugin.getDiscord().defaultChannel);
			}
		}
	}

	public boolean isServer(Server s){
		return s.getId().equalsIgnoreCase(plugin.getDiscord().serverId);
	}
}
