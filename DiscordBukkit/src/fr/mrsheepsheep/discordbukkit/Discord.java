package fr.mrsheepsheep.discordbukkit;

import java.util.HashMap;
import java.util.UUID;

import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import me.itsghost.jdiscord.DiscordAPI;
import me.itsghost.jdiscord.DiscordBuilder;
import me.itsghost.jdiscord.Server;
import me.itsghost.jdiscord.exception.BadUsernamePasswordException;
import me.itsghost.jdiscord.exception.DiscordFailedToConnectException;
import me.itsghost.jdiscord.exception.NoLoginDetailsException;
import me.itsghost.jdiscord.message.Message;
import me.itsghost.jdiscord.message.MessageBuilder;
import me.itsghost.jdiscord.talkable.Group;

public class Discord {

	public DiscordAPI api;
	DiscordBukkit plugin;

	String serverId, email, password;
	String defaultChannel, defaultChannelId, inviteId;
	String toIgnore;

	HashMap<UUID, String> joinedChannels = new HashMap<UUID, String>();
	HashMap<UUID, DiscordAPI> loggedInPlayers = new HashMap<UUID, DiscordAPI>();

	public Discord(DiscordBukkit plugin){
		this.plugin = plugin;
	}

	public Integer numberIn(String id) {

		int i = 0;
		for (String s : joinedChannels.values()){
			if (s.equalsIgnoreCase(id))
				i++;
		}
		return i;
	}

	public Server getDiscordServer(){
		return api.getServerById(serverId);
	}

	public String getChannelID(String name){
		for (Group g : getDiscordServer().getGroups()){
			if (g.getName().equalsIgnoreCase(name))
				return g.getId();
		}
		return null;
	}

	public Group getPlayerGroup(UUID uuid){
		if (loggedInPlayers.containsKey(uuid)){
			DiscordAPI api = loggedInPlayers.get(uuid);
			api.joinInviteId(inviteId);
			for (Group group : api.getServerById(serverId).getGroups()){
				if (group.getId().equalsIgnoreCase(joinedChannels.get(uuid)))
					return group;
			}
		}
		else
		{
			for (Group group : getDiscordServer().getGroups()){
				if (group.getId().equalsIgnoreCase(joinedChannels.get(uuid)))
					return group;
			}

			if (getDiscordServer().getGroupById(defaultChannelId) != null)
				return getDiscordServer().getGroupById(defaultChannelId);
		}
		return getDiscordServer().getGroups().get(0);
	}

	public DiscordAPI getApi(){
		return api;
	}

	public String getServerId(){
		return serverId;
	}

	public boolean joinChannel(UUID uuid, String channelName) {
		for (Group g : getDiscordServer().getGroups()){
			if (g.getName().equalsIgnoreCase(channelName)){
				joinedChannels.put(uuid, g.getId());
				return true;
			}
		}
		joinedChannels.put(uuid, defaultChannelId);
		return false;
	}

	public void sendMessage(final CommandSender sender, final String channel, final String toSend){

		plugin.getServer().getScheduler().scheduleSyncDelayedTask(plugin, new Runnable(){

			DiscordAPI API = api;

			@Override
			public void run() {
				
				String msg = toSend;

				if (sender instanceof Player){
					Player p = (Player) sender;
					if (!loggedInPlayers.containsKey(p.getUniqueId()))
						msg = "__" + p.getName() + "__" + ": " + toSend;
					else
					{
						API = loggedInPlayers.get(p.getUniqueId());
					}
				}
				else
				{
					msg = "**Console**: " + toSend;
				}

				Message message = new MessageBuilder().addString(msg).build();
				toIgnore = message.getMessage();
				
				Server s = API.getServerById(serverId);
				Group g = s.getGroupById(channel);
				if (g == null){
					for (Group group : s.getGroups()){
						if (group.getName().equalsIgnoreCase(channel)){
							g = group;
						}
					}
					if (g == null){
						g = s.getGroups().get(0);
					}
				}
				
				g.sendMessage(message);
			}

		}, 0L);
	}

	public boolean loadAPI(){
		if (!(serverId.isEmpty() && email.isEmpty() && password.isEmpty() && inviteId.isEmpty())){
			try {
				api = new DiscordBuilder(email, password).build().login();
				api.getEventManager().registerListener(new DiscordChatListener(plugin));
			} catch (NoLoginDetailsException e) {
				plugin.getLogger().severe("No login details provided !");
				return false;
			} catch (BadUsernamePasswordException e) {
				plugin.getLogger().severe("Bad login :(");
				return false;
			} catch (DiscordFailedToConnectException e) {
				plugin.getLogger().severe("Failed to connect to Discord !");
				return false;
			}
		}
		else
		{
			plugin.getLogger().warning("Configuration is not complete. Disabling.");
			return false;
		}
		return true;
	}

}
