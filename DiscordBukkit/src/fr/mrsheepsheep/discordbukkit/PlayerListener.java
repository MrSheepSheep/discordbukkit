package fr.mrsheepsheep.discordbukkit;

import java.util.UUID;

import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.event.player.PlayerCommandPreprocessEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;

import me.itsghost.jdiscord.DiscordAPI;
import me.itsghost.jdiscord.DiscordBuilder;
import me.itsghost.jdiscord.exception.BadUsernamePasswordException;
import me.itsghost.jdiscord.exception.DiscordFailedToConnectException;
import me.itsghost.jdiscord.exception.NoLoginDetailsException;
import me.itsghost.jdiscord.message.Message;
import me.itsghost.jdiscord.message.MessageBuilder;

public class PlayerListener implements Listener {

	DiscordBukkit plugin;

	public PlayerListener(DiscordBukkit plugin){
		this.plugin = plugin;
		plugin.getServer().getPluginManager().registerEvents(this, plugin);
	}

	@EventHandler
	public void onPlayerChat(final AsyncPlayerChatEvent e){

		final Player p = e.getPlayer();
		final UUID uuid = p.getUniqueId();
		if (plugin.getDiscord().joinedChannels.containsKey(uuid)){
			plugin.getServer().getScheduler().scheduleSyncDelayedTask(plugin, new Runnable(){
				@Override
				public void run() {
					plugin.getDiscord().sendMessage(p, plugin.getDiscord().getPlayerGroup(uuid).getId(), e.getMessage());
					//plugin.getDiscord().getPlayerGroup(uuid).sendMessage(message);
				}

			}, 0L);
		}
		else
		{
			e.setCancelled(true);
			p.sendMessage(ChatColor.RED + "Please join a channel using /discord join <channel>");
		}
	}

	@EventHandler
	public void onPlayerJoin(PlayerJoinEvent e){
		plugin.getDiscord().joinChannel(e.getPlayer().getUniqueId(), plugin.getDiscord().defaultChannelId);
		e.getPlayer().sendMessage(ChatColor.GRAY + "You are now talking in channel #" + plugin.getDiscord().defaultChannel);

		if (plugin.getDiscord().loggedInPlayers.containsKey(e.getPlayer().getUniqueId())){
			try {
				DiscordAPI api = plugin.getDiscord().loggedInPlayers.get(e.getPlayer().getUniqueId());
				api.login();
				api.getAccountManager().setOnlineStatus(true);
			} catch (NoLoginDetailsException e1) {
				e1.printStackTrace();
			} catch (BadUsernamePasswordException e1) {
				e1.printStackTrace();
			} catch (DiscordFailedToConnectException e1) {
				e1.printStackTrace();
			}
		}
	}

	@EventHandler
	public void onPlayerQuit(PlayerQuitEvent e){
		if (plugin.getDiscord().loggedInPlayers.containsKey(e.getPlayer().getUniqueId())){
			DiscordAPI api = plugin.getDiscord().loggedInPlayers.get(e.getPlayer().getUniqueId());
			api.getAccountManager().setOnlineStatus(false);
			api.stop();
		}
	}

	/* Bypassing main command to hide password in console / logs */
	@EventHandler
	public void onLoginCommand(PlayerCommandPreprocessEvent e){
		String[] args = e.getMessage().split(" ");
		if (args.length >= 2){
			if (args[0].equalsIgnoreCase("/discord") || args[0].equalsIgnoreCase("/d") || args[0].equalsIgnoreCase("/discordbukkit") || args[0].equalsIgnoreCase("/dis") || args[0].equalsIgnoreCase("/db")){
				if (args[1].equalsIgnoreCase("login")){
					if (args.length == 4){
						if (plugin.getDiscord().loggedInPlayers.containsKey(e.getPlayer().getUniqueId())){
							e.getPlayer().sendMessage(ChatColor.RED + "You are already connected to Discord. Type /discord logout to logout.");
						}
						else
						{
							try {
								DiscordAPI login = new DiscordBuilder(args[2], args[3]).build().login();
								login.joinInviteId(plugin.getDiscord().inviteId);
								plugin.getDiscord().loggedInPlayers.put(e.getPlayer().getUniqueId(), login);
								e.getPlayer().sendMessage(ChatColor.GREEN + "Connected !");
							} catch (NoLoginDetailsException e1) {
								e1.printStackTrace();
							} catch (BadUsernamePasswordException e1) {
								e.getPlayer().sendMessage(ChatColor.RED + "Your email or password is incorrect.");
							} catch (DiscordFailedToConnectException e1) {
								e.getPlayer().sendMessage(ChatColor.RED + "Failed to connect to Discord.");
							}
						}
					}
					else
					{
						e.getPlayer().sendMessage(ChatColor.RED + "Type /discord login <email> <password> to login.");
					}
					e.setCancelled(true);
				}
			}
		}
	}
}
