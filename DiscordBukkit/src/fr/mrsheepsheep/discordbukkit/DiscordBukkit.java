package fr.mrsheepsheep.discordbukkit;

import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.event.Listener;
import org.bukkit.plugin.java.JavaPlugin;

import me.itsghost.jdiscord.DiscordAPI;

public class DiscordBukkit extends JavaPlugin implements Listener {

	public static Discord discord;

	public void onEnable() {
		
		discord = new Discord(this);
		
		loadConfiguration();
		if (discord.loadAPI()){
			new DiscordCommand(this);
			new PlayerListener(this);
			
			sendOnlineHeartBeat();
		}
		else
			getServer().getPluginManager().disablePlugin(this);
	}

	public void onDisable(){
		getServer().getScheduler().cancelTasks(this);	
	}

	public void loadConfiguration(){
		FileConfiguration c = getConfig();
		c.options().copyDefaults(true);
		saveConfig();
		discord.defaultChannel = c.getString("defaultChannelName");
		discord.serverId = c.getString("serverID");
		discord.email = c.getString("email");
		discord.password = c.getString("password");
		discord.inviteId = c.getString("inviteID");
	}

	public void cancelTask(int id){
		getServer().getScheduler().cancelTask(id);
	}

	public void sendOnlineHeartBeat(){
		getServer().getScheduler().scheduleSyncRepeatingTask(this, new Runnable(){

			@Override
			public void run() {

			}

		}, 20L * 10, 20L * 5);
		for (DiscordAPI api : discord.loggedInPlayers.values()){
			if (api.isLoaded())
				api.getAccountManager().setOnlineStatus(true);
		}
	}
	
	public Discord getDiscord(){
		return discord;
	}



}
